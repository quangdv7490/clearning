#include<stdio.h>
#include<conio.h>

int main()
{
	float dToan, dLy, dHoa;
	int hsToan, hsLy, hsHoa;
	float diemtrungbinh;

	printf_s("\nNhap diem toan: ");
	scanf_s("%f", &dToan);
	printf_s("\nNhap he so toan: ");
	scanf_s("%d", &hsToan);

	printf_s("\nNhap diem ly: ");
	scanf_s("%f", &dLy);
	printf_s("\nNhap he so ly: ");
	scanf_s("%d", &hsLy);

	printf_s("\nNhap diem hoa: ");
	scanf_s("%f", &dHoa);
	printf_s("\nNhap he so hoa: ");
	scanf_s("%d", &hsHoa);

	diemtrungbinh = (dToan * hsToan + dLy * hsLy + dHoa * hsHoa) / (hsToan + hsLy + hsHoa);

	printf_s("\nDiem trung binh la: %.2f", diemtrungbinh);

	_getch();
	return 0;
}