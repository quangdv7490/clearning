//Giai phuong trinh bac nhat : ax + b = 0;
#include <stdio.h>
#include <conio.h>

int main()
{
	int a, b;

	printf_s("\nNhap a: ");
	scanf_s("%d", &a);

	printf_s("\nNhap b: ");
	scanf_s("%d", &b);

	if (a == 0)
	{
		if (b == 0)
		{
			printf_s("\nPhuong trinh vo so nghiem");
		}
		else
		{
			printf_s("\nPhuong trinh vo nghiem");
		}
	}
	else
	{
		printf_s("\nPhuong trinh co nghiem duy nhat la: %f", -(float)b / a);
	}

	_getch();
	return 0;
}