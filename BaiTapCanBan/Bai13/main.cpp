/*
Giai phuong trinh bac 2: ax2 + bx + c = 0
Trong do a khac 0 - b la he so - c la hang so
*/

#include<stdio.h>
#include<conio.h>
#include<math.h>

int main()
{
	int a, b, c;
	float delta;

	printf_s("\nNhap a: ");
	scanf_s("%d", &a);

	printf_s("\nNhap b: ");
	scanf_s("%d", &b);

	printf_s("\nNhap c: ");
	scanf_s("%d", &c);

	delta = pow((float)b, 2) - (4 * a * c);

	if (delta > 0)
	{
		float x1 = (-b - sqrt(delta)) / (2 * a);
		float x2 = (-b + sqrt(delta)) / (2 * a);
		printf_s("\nPhuong trinh co 2 nghiem phan biet x1 = %f - x2 = %f", x1, x2);
	}

	if (delta == 0)
	{
		float x = -b / (2 * a);
		printf_s("\nPhuong trinh co nghiem duy nhat x = %f", x);
	}

	if (delta < 0)
	{
		printf_s("\nPhuong trinh vo nghiem");
	}

	_getch();
	return 0;
}