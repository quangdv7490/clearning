﻿
#include<stdio.h>
#include<conio.h>

int main()
{
	int soLuong, donGia, tien;
	float thue, tongTien;

	printf_s("\nNhap so luong: ");
	scanf_s("%d", &soLuong);

	printf_s("\nNhap don gia: ");
	scanf_s("%d", &donGia);

	tien = soLuong * donGia;
	thue = tien * 0.1;

	printf_s("\nTien phai tra la: %d", tien);
	printf_s("\nThue gia tri gia tang: %.3f", thue);
	tongTien = (float)tien + thue;
	printf_s("\nTong tien phai tra: %.3f", tongTien);

	_getch();
	return 0;
}