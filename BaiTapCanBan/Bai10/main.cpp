#include<stdio.h>
#include<conio.h>

#define pi 3.14;

int main()
{
	float r, chuvi, dientich;

	printf_s("\nNhap ban kinh duong tron: ");
	scanf_s("%f", &r);

	chuvi = 2 * r * pi;

	dientich = r * r * pi;

	printf_s("\nChu vi hinh tron la: %f", chuvi);

	printf_s("\nDien tich hinh tron la: %f", dientich);

	_getch();
	return 0;
}