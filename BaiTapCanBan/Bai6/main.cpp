﻿//Nhập năm sinh của một người và tính tuổi của người đó
#include<stdio.h>
#include<conio.h>
#include<time.h>

int main()
{
	int namSinh, namHienTai, tuoi;

	//namHienTai = 2016;
	time_t rawtime;
	struct tm timeinfo;
	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);
	namHienTai = timeinfo.tm_year + 1900;

	printf_s("\nNhap nam sinh cua ban: ");
	scanf_s("%d", &namSinh);

	tuoi = namHienTai - namSinh;

	printf_s("\nTuoi cua ban la: %d", tuoi);

	_getch();
	return 0;
}