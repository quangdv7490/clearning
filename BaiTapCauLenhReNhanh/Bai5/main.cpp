/*
 * Viet chuong trinh nhap vao gio bat dau - gio ket thuc va in ra so tien khach hang phai tra
 * Biet rang 8 <= Gio bat dau < gio ket thuc <= 24
 * Tien duoc tinh theo cong thuc sau
 * Moi gio trong 3 gio dau tien tinh 30.000 dong/gio
 * Moi gio tiep theo co don gia giam 30% so voi don gia trong 3 gio dau tien
 * Ngoai ra neu thoi gian thue phong tu 8 - 17 gio thi duoc giam gia 10%.
 */

#include<stdio.h>
#include<conio.h>

int main()
{
	int vGioBatDau, vGioKetThuc, vGioHat;
	const float vTienMoiGio = 30000;

	printf_s("\nNhap vao gio bat dau: ");
	scanf_s("%d", &vGioBatDau);
	printf_s("\nNhap vao gio ket thuc: ");
	scanf_s("%d", &vGioKetThuc);

	if (vGioBatDau < 8 || vGioBatDau > vGioKetThuc || vGioKetThuc > 24)
	{
		printf_s("\nVui long kiem tra lai gio bat dau hoac gio ket thuc!");
	}
	else
	{
		float vTienThanhToan;
		vGioHat = vGioKetThuc - vGioBatDau;
		if (vGioHat <= 3)
		{
			vTienThanhToan = vGioHat * vTienMoiGio;
		}
		else if (vGioHat > 3 && vGioHat < 9)
		{
			vTienThanhToan = (vGioHat * vTienMoiGio) - (3 * vTienMoiGio * 0.3);
		}
		else if (vGioHat >= 9)
		{
			vTienThanhToan = (vGioHat * vTienMoiGio) - (vGioHat * vTienMoiGio * 0.1);
		}

		printf_s("\nSo gio khach hat la: %d", vGioHat);
		printf_s("\nSo tien khach hang can thanh toan la: %.3f dong", vTienThanhToan);
	}

	_getch();
	return 0;
}