/*
* Viet chuong trinh tinh luong nhan vien dua theo tham nien cong tac(TNCC)
* Luong = heso * luongcanban. trong do luong can ban la 650000 dong
* Neu TNCC < 12 thang: heso = 1.92
* Neu 12 <= TNCC < 36 thang: heso = 2.34
* Neu 36 <= TNCC < 60 thang: heso = 3
* Neu TNCC >= 60 thang: heso = 4.5
*/

#include<stdio.h>
#include<conio.h>

//Dinh nghia luong co ban
#define luongcoban 650000

int main()
{
	int TNCT;

	printf_s("\nNhap tham nien cong tac: ");
	scanf_s("%d", &TNCT);

	if (TNCT < 0)
	{
		TNCT *= -1;
	}

	float heSoLuong;
	if (TNCT < 12)
	{
		heSoLuong = 1.92;
	}
	else if (TNCT >= 12 && TNCT < 36)
	{
		heSoLuong = 2.34;
	}
	else if (TNCT >= 36 && TNCT < 60)
	{
		heSoLuong = 3;
	}
	else
	{
		heSoLuong = 4.5;
	}

	float luong = heSoLuong * luongcoban;

	printf_s("Luong nhan duoc la: %f", luong);

	_getch();
	return 0;
}