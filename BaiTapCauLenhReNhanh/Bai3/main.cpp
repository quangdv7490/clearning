/*
 * Viet chuong trinh nhap vao so co ba chu so
 * In ra cach doc so nay. Vi du: 123 => Mot tram hai muoi ba
 */

#include<stdio.h>
#include<conio.h>
#include<math.h>

int main()
{
	int n;

	printf_s("\nNhap vao mot so co ba chu so: ");
	scanf_s("%d", &n);

	int check = 0;
	if (n < 0)
	{
		n *= -1;
		check = 1;
	}

	int soChuSo = log10((double)n) + 1;

	if (soChuSo == 3)
	{
		int hangTram = n / 100;
		int hangChuc = (n % 100) / 10;
		int hangDonVi = (n % 100) % 10;

		if (check == 1)
		{
			printf_s("Am ");
		}

		if (hangTram == 1)
		{
			printf_s("Mot Tram ");
		}
		else if (hangTram == 2)
		{
			printf_s("Hai Tram ");
		}
		else if (hangTram == 3)
		{
			printf_s("Ba Tram ");
		}
		else if (hangTram == 4)
		{
			printf_s("Bon Tram ");
		}
		else if (hangTram == 5)
		{
			printf_s("Nam Tram ");
		}
		else if (hangTram == 6)
		{
			printf_s("Sau Tram ");
		}
		else if (hangTram == 7)
		{
			printf_s("bay Tram ");
		}
		else if (hangTram == 8)
		{
			printf_s("Tam Tram ");
		}
		else if (hangTram == 9)
		{
			printf_s("Chin Tram ");
		}

		//In ra hang chuc
		if (hangChuc == 1)
		{
			printf_s("Muoi ");
		}
		else if (hangChuc == 2)
		{
			printf_s("Hai Muoi ");
		}
		else if (hangChuc == 3)
		{
			printf_s("Ba Muoi ");
		}
		else if (hangChuc == 4)
		{
			printf_s("Bon Muoi ");
		}
		else if (hangChuc == 5)
		{
			printf_s("Nam Muoi ");
		}
		else if (hangChuc == 6)
		{
			printf_s("Sau Muoi ");
		}
		else if (hangChuc == 7)
		{
			printf_s("Bay Muoi ");
		}
		else if (hangChuc == 8)
		{
			printf_s("Tam Muoi ");
		}
		else if (hangChuc == 9)
		{
			printf_s("Chin Muoi ");
		}

		//In ra hang don vi
		if (hangDonVi == 1)
		{
			printf_s("Mot ");
		}
		else if (hangDonVi == 2)
		{
			printf_s("Hai ");
		}
		else if (hangDonVi == 3)
		{
			printf_s("Ba ");
		}
		else if (hangDonVi == 4)
		{
			printf_s("Bon ");
		}
		else if (hangDonVi == 5)
		{
			printf_s("Nam ");
		}
		else if (hangDonVi == 6)
		{
			printf_s("Sau ");
		}
		else if (hangDonVi == 7)
		{
			printf_s("Bay ");
		}
		else if (hangDonVi == 8)
		{
			printf_s("Tam ");
		}
		else if (hangDonVi == 9)
		{
			printf_s("Chin ");
		}

	}
	else
	{
		printf_s("\nSo nhap vao phai la so co ba chu so, vui long kiem tra lai!");
	}

	_getch();
	return 0;
}