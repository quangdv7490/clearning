/*
 * Viet chuong trinh nhap vao 4 so.
 * In ra man hinh so lon nhat va so nho nhat
 */

#include<stdio.h>
#include<conio.h>

int main()
{
	//Declare Variables
	int a, b, c, d, max, min;

	//Input
	printf_s("\nNhap a: ");
	scanf_s("%d", &a);

	printf_s("\nNhap b: ");
	scanf_s("%d", &b);

	printf_s("\nNhap c: ");
	scanf_s("%d", &c);

	printf_s("\nNhap d: ");
	scanf_s("%d", &d);

	//Process
	/*Cach 1*/
	/*max = a;
	if (b > max)
	{
		max = b;
	}
	if (c > max)
	{
		max = c;
	}
	if (d > max)
	{
		max = d;
	}

	min = a;
	if (b < min)
	{
		min = b;
	}
	if (c < min)
	{
		min = c;
	}
	if (d < min)
	{
		min = d;
	}*/

	/*Cach 2*/
	min = a < b ? a : b;
	min = min < c ? min : c;
	min = min < d ? min : d;

	max = a > b ? a : b;
	max = max > c ? max : c;
	max = max > d ? max : d;

	//Output
	printf_s("\nSo lon nhat la: %d", max);
	printf_s("\nSo nho nhat la: %d", min);

	//End
	_getch();
	return 0;
}