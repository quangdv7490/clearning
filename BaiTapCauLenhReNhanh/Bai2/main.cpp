/*
 * Viet chuong trinh nhap vao mot so co 2 chu so
 * In ra cach doc cua so nay. Vi du: 12 => Muoi hai
 */

#include<stdio.h>
#include<conio.h>
#include<math.h>

int main()
{
	int n;

	printf_s("\nNhap vao mot so: ");
	scanf_s("%d", &n);

	int check = 0; //Dat co hieu de kiem tra
	if (n < 0)
	{
		check = 1;		//Bat co hieu toi 1
		n *= -1;		//Bien n thanh so duong
	}

	int soChuSo = log10((double)n) + 1;		//Tinh so chu so

	if (soChuSo == 2)
	{
		int hangChuc = n / 10;
		int hangDonVi = n % 10;

		//In ra am neu so nhap vao la so am
		if (check == 1)
		{
			printf_s("Am ");
		}

		//In ra hang chuc
		if (hangChuc == 1)
		{
			printf_s("Muoi ");
		}
		else if (hangChuc == 2)
		{
			printf_s("Hai Muoi ");
		}
		else if (hangChuc == 3)
		{
			printf_s("Ba Muoi ");
		}
		else if (hangChuc == 4)
		{
			printf_s("Bon Muoi ");
		}
		else if (hangChuc == 5)
		{
			printf_s("Nam Muoi ");
		}
		else if (hangChuc == 6)
		{
			printf_s("Sau Muoi ");
		}
		else if (hangChuc == 7)
		{
			printf_s("Bay Muoi ");
		}
		else if (hangChuc == 8)
		{
			printf_s("Tam Muoi ");
		}
		else if (hangChuc == 9)
		{
			printf_s("Chin Muoi ");
		}

		//In ra hang don vi
		if (hangDonVi == 1)
		{
			printf_s("Mot ");
		}
		else if (hangDonVi == 2)
		{
			printf_s("Hai ");
		}
		else if (hangDonVi == 3)
		{
			printf_s("Ba ");
		}
		else if (hangDonVi == 4)
		{
			printf_s("Bon ");
		}
		else if (hangDonVi == 5)
		{
			printf_s("Lam ");
		}
		else if (hangDonVi == 6)
		{
			printf_s("Sau ");
		}
		else if (hangDonVi == 7)
		{
			printf_s("Bay ");
		}
		else if (hangDonVi == 8)
		{
			printf_s("Tam ");
		}
		else if (hangDonVi == 9)
		{
			printf_s("Chin ");
		}
	}
	else
	{
		printf_s("\nSo chu so phai la 2 chu so, vui long kiem tra lai!");
	}

	//printf_s("\nSo chu so: %d", soChuSo);

	_getch();
	return 0;
}