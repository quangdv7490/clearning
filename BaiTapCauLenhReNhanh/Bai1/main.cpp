/*
 * Nhap vao 2 so. Tim so lon nhat va so nho nhat
 */

#include<stdio.h>
#include<conio.h>

int main()
{
	int a, b, max, min;

	printf_s("\nNhap so thu nhat: ");
	scanf_s("%d", &a);

	printf_s("\nNhap so thu hai: ");
	scanf_s("%d", &b);

	if (a > b)
	{
		max = a;
		min = b;
	}
	else
	{
		max = b;
		min = a;
	}

	printf_s("\nSo lon nhat la: %d", max);
	printf_s("\nSo nho nhat la: %d", min);

	_getch();
	return 0;
}