/*
 * Viet chuong trinh tinh luong nhan vien dua theo tham nien cong tac(TNCC)
 * Luong = heso * luongcanban. trong do luong can ban la 650000 dong
 * Neu TNCC < 12 thang: heso = 1.92
 * Neu 12 <= TNCC <= 36 thang: heso = 2.34
 * Neu 36 <= TNCC <= 60 thang: heso = 3
 * Neu TNCC > 60 thang: heso = 4.5 
 */

#include<stdio.h>
#include<conio.h>

int main()
{
	int vThang;

	const float luongCoBan = 650000;
	const float heso_4 = 4.5;
	const float heso_3 = 3;
	const float heso_2 = 2.34;
	const float heso_1 = 1.92;

	printf_s("\nNhap Tham Nien Cong Tac(so thang): ");
	scanf_s("%d", &vThang);

	if (vThang < 0)
	{
		vThang *= -1;
	}

	float vLuong;
	if (vThang > 60)
	{
		vLuong = heso_4 * luongCoBan;
	}
	else if (vThang >= 36 && vThang <= 60)
	{
		vLuong = heso_3 * luongCoBan;
	}
	else if (vThang >=  12 && vThang <= 36)
	{
		vLuong = heso_2 * luongCoBan;
	}
	else
	{
		vLuong = heso_1 * luongCoBan;
	}

	printf_s("\nLuong theo tham nien %d thang la: %f", vThang, vLuong);

	_getch();
	return 0;
}